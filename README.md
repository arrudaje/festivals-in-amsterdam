# AMS Fest #

The AMS Fest is a project to make Festivals in Amsterdam widely known. There you can find the festivals currently ongoing and see their locations in order to know where to rock!

The project was developed with AngularJS accessing the CitySDK APIs via HTTP request, using also the Google Maps API to display the map to locate the festivals and Scss as a preprocessor for CSS. The logo, slogan, icons and page layout were all made by me, keeping in mind the Amsterdam spirit, taking in consideration its flag, its colours and the pleasantness of the life in the city.
I intended to make it simple, as simple as it is in Backbase platform, and also practical. The main challenge was to display the location description without covering up the map, to maximize the user experience, but with a simple collapse button I could manage to make both live together.