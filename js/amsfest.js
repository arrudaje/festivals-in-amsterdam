var map;
var marker;
function initMap() {
	function initialize(latitude, longitude){
		//start map and center to current location
		map = new google.maps.Map(document.querySelector('div.map'), {
			center: {lat: latitude, lng: longitude},
			zoom: 15
		});

		marker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            icon: 'img/marker.png',
            map: map
          });
	}

	navigator.geolocation.getCurrentPosition(
		function(pos){ //success
			posLat = parseFloat(pos.coords.latitude);
			posLon = parseFloat(pos.coords.longitude);
			initialize(posLat, posLon);
		}, 
		function(err){ //error
			posLat = -23.6815315;
           	posLon = -46.8754819;
           	initialize(posLat, posLon);
		} 
	);
}

function SVGSupport(){
	return typeof SVGRect != "undefined";
}

function removeExtension(string){
	return string.slice(0, -4);
}

function clearMarkers(){
	marker.setMap(null);
}

function setMarker(lat, lng){
	marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        icon: 'img/fest.png',
        map: map
    });

    map.setCenter(new google.maps.LatLng(lat, lng));
}

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

document.addEventListener("DOMContentLoaded", function(){
	/*Checks if browser supports SVG. If it doesn't, images in SVG turn into PNG.*/
	if(!SVGSupport()){
		var imgs = document.querySelectorAll("img[src$=.svg]");
		for(img in imgs){
			imgs[img].setAttribute("src", removeExtension(imgs[img].getAttribute("src")) + ".png");
		}
	}

	document.querySelector(".arrow").addEventListener("click", function(){ //when user clicks on the arrow the description div collapses so they can see the map properly
		if(hasClass(this, "collapse")){
			//hide text to avoid overbreaking
			var text = this.parentNode.querySelectorAll("h1, p");
			for(t in text){
				if(text[t].tagName) text[t].style.display = "none";
			}

			this.parentNode.style.width = "20px";
			this.className = "arrow expand";
		}
		else if(hasClass(this, "expand")){
			this.parentNode.style.width = this.parentNode.parentNode.style.width;
			console.log(this.parentNode.parentNode.style.width);
			this.className = "arrow collapse";
			//show text again
			var text = this.parentNode.querySelectorAll("h1, p");
			for(t in text){
				if(text[t].tagName) text[t].style.display = "block";
			}
		}
	});
});