module.controller("FestivalsController", function($scope, Festivals, Locations){
	Festivals.getAll().then(
		function(festivals){
			$scope.festivals = festivals; //get list of festivals
		},
		function(error){
			alert("Error on retrieving festivals");
			console.log(error);
		}
	);

	$scope.selectFestival = function(locationId){
		Locations.get(locationId).then(
			function(location){
				$scope.selected = location; //get location object
				console.log($scope.selected);
				clearMarkers(); //clear previous marker from map
				var position = Locations.getPosition(location);
				setMarker(position.lat, position.lng); //set new marker and center the map on it
			},
			function(error){
				alert("Error on retrieving festival location");
				console.log(error);
			}
		);
	}

	
});