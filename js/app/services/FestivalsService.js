angular.module("FestivalsService", [])

.factory("Festivals", ['$http', '$q', function($http, $q){

    return {
        // call to get request
        getAll : function(){
            var defer = $q.defer();
            $http.get('http://citysdk.dmci.hva.nl/CitySDK/events/search?category=festival').then(
            	function(d){ //successCallback
                	defer.resolve(d.data.event);
            	},
            	function(d){ //errorCallback
            		defer.reject(d);
            	});
            return defer.promise;
        }
    }
}]);