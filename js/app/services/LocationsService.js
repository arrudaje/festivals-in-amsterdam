angular.module("LocationsService", [])

.factory("Locations", ['$http', '$q', function($http, $q){

    return {
        // call to get request
        get : function(id){
            var defer = $q.defer();
            $http.get('http://citysdk.dmci.hva.nl/CitySDK/pois/' + id).then(
            	function(d){ //successCallback
                	defer.resolve(d.data);
            	},
            	function(d){ //errorCallback
            		defer.reject(d);
            	});
            return defer.promise;
        },

        getPosition: function(location){ //slice position string from original object and return proper latitude and longitude
            var position = {};
            var point = location.location.point[0].Point.posList;
            position.lat = point.slice(0, point.indexOf(" ") - 1);
            position.lng = point.slice(point.indexOf(" ") + 1);
            console.log(position);
            return position;
        }
    }
}]);